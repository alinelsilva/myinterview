package com.example;

import java.util.Scanner;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
	
	public static void main(String[] args) {
		
		String palavra, palavraAux = "";
		
		//ler a palavra pelo teclado
		Scanner s = new Scanner(System.in);
		
		//armaneza a palavra numa string
		System.out.print("Digite uma palavra: ");
		palavra = s.nextLine().toLowerCase();
		
		for(int i = palavra.length() -1; i>=0; i--) {
			palavraAux += palavra.charAt(i);
		}
		
		if(palavra.equals(palavraAux)) {
			System.out.println("É uma palavra/frase palindromo");
			
		} else {
			System.out.println("Não é uma palavra/frase palindromo");

		}
		
	}

}
