package com.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 {
	public static void main(String[] args) {
	    Scanner ler = new Scanner(System.in);

	    ArrayList<String> lista = new ArrayList();

	    lista.add("02");
	    lista.add("10");
	    lista.add("14");
	    lista.add("06");
	    lista.add("04");
	    lista.add("20");
	    lista.add("22");
	    lista.add("42");
	    lista.add("32");
	    lista.add("25");

//	    int i;
		gerarLista(lista);
	    
		
		 System.out.printf("Posicao removida: " +  lista.remove(5) +"\n\n");
	   
	    
	    gerarLista(lista);
	    
	    
	    System.out.printf("Numero removido: " +  lista.remove(7) +"\n\n");
	   
	    
	    gerarLista(lista);
	    

	  }

	private static void gerarLista(ArrayList<String> lista) {
		int i;

	    System.out.println("############################");
	    System.out.println("Lista\n");
	    for (i=0; i<lista.size(); i++) {
	      System.out.printf("Posição %d- %s\n", i, lista.get(i));
	    }
	    
	    System.out.println("\n############################");
	}

}
