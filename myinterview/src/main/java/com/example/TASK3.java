package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
	   public static void main(String[] args) {
		   
	        Random numeroAlet = new Random();
	        int listaNumero[] = new int[10], resp=0;
	        
	        Arrays.sort(listaNumero);

	        System.out.println("############################");
	        for (int i = 0; i < listaNumero.length; i++) {
	        	listaNumero[i] = numeroAlet.nextInt(20);
	        	System.out.println(listaNumero[i]);
	        	
	        	for(int j =i + 1; j<listaNumero.length; j++) {
	        		if(listaNumero[j] == listaNumero[i]) {
	        			resp=1;
	        		}
	        	}
	            
	         }
	        System.out.println("Numeros Distintos: " + resp);

	        System.out.println("############################");
	        
	        
	    }

}
